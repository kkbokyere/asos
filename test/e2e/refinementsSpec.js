describe('Asos Refinements App', function() {
	beforeEach(function() {
		browser.get('/');
	});
	describe("Initialisation", function() {
		it('should init template view', function() {
			var refinementsPanel = element(by.css('.refinements'));
			expect(element(by.id('refinements-title')).getText()).toBe('REFINE BY');
			expect(refinementsPanel.isPresent()).toBeTruthy();
		});
		it("should load a view with panel of sets of 3 facets", function() {
			var facetsList = element.all(by.repeater('facet in facets'));
			expect(facetsList.count()).toEqual(3);
		});

		it("should load a view with properties in facets", function() {
			element.all(by.css('.options-list li input')).then(function(items) {
				expect(items.length).toBeGreaterThan(1);
			});
		});
	});

	describe("Interaction", function() {
		it("should allow individual facets to be selected and de-selected by clicking on the various checkboxes", function() {
			var facetsList = element.all(by.repeater('facet in facets'));
			var selectedInput = facetsList.get(1).element(by.css('.options-list-item input:nth-child(1)'));
			selectedInput.click();
		});
		it("should deselect all the facets in the a given panel and hides the 'clear' button when i click 'clear'", function() {
			var sizeClearFilter = $('[data-clear="size"]');
			var sizeFourInput = $('input#size_4');
			sizeFourInput.click();
			expect(sizeFourInput.isSelected()).toBeTruthy();
			sizeClearFilter.click();
			expect(sizeFourInput.isSelected()).not.toBeTruthy();
			expect(sizeClearFilter.isDisplayed()).not.toBeTruthy();
		});
		it("should deselect all facets across all the panels and hides all the 'clear' and the 'clear all' buttons", function() {
			var allClearFilter = element(by.css('[data-clear="all"]'));
			var clearFilters = element.all(by.css('[data-clear]'));
			var inputs = element.all(by.css('input[type="checkbox"]:checked'));
			inputs.click();
			expect(inputs.isSelected()).toBeTruthy();
			allClearFilter.click();
			expect(inputs.count()).toBe(0);
			//expect(clearFilters.isDisplayed()).toBe(false)
		});
	});

	describe("Deserializing a url string", function() {
		it("should deserialise one refine value and select the size uk 4", function() {
			var facetEl = $('input#size_4');
			browser.setLocation('?refine=size:4');
			expect(browser.getCurrentUrl()).toBe('http://localhost:8000/?refine=size:4');
			expect(facetEl.isSelected()).toBeTruthy();
		});

		it("should deserialise one refine value and select the size uk 4", function() {
			browser.setLocation('?refine=size:4,10');
			expect(browser.getCurrentUrl()).toBe('http://localhost:8000/?refine=size:4,10');
			expect($('input#size_4').isSelected()).toBeTruthy();
			expect($('input#size_10').isSelected()).toBeTruthy();
		});
		it("should deserialise multiple refine values and select the size uk 4 and color 1", function() {
			browser.setLocation('?refine=size:4|colour:1|brand:3098');
			expect($('input#size_4').isSelected()).toBeTruthy();
			expect($('input#base_colour_1').isSelected()).toBeTruthy();
			expect($('input#brand_3098').isSelected()).toBeTruthy();
		});
	});

});