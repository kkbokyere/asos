describe('refinementsCtrl', function(){
    var scope,
        model,
        $templateCache,$compile,
        ctrl;

    beforeEach(module('asosRefinementsApp'));

    beforeEach(inject(function($rootScope, $controller, _facetsListModel_,_$templateCache_, _$compile_) {
        $templateCache = _$templateCache_;
        $compile = _$compile_;
        scope = $rootScope.$new();
        model = _facetsListModel_;
        ctrl = $controller('asosRefinementsCtrl', {$scope: scope});
    }));

    it('should set the scope of the refinements controller', function() {
        expect(scope.facets).toBe(model);
    });
});