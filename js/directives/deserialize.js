/**
 * @function deserialize
 * @memberOf asosRefinementsApp
 * @description this deserializes the query string in the URL and then it selects the appropriate checkboxes.
 */
angular.module('asosRefinementsApp').directive('deserialize', function($timeout) {
    return {
        link: function(scope) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('deserialize');
                });
            }
        }};
});