/**
 * @function clearFilter
 * @memberOf asosRefinementsApp
 * @description click event bound to clear and clear all button to uncheck its children input elements.
 */
angular.module('asosRefinementsApp').directive('clearFilter', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('click', function(event) {
                $(element)
                    .parent()
                    .nextAll()
                    .find(':checked')
                    .attr('checked', false)
                    .change()
                    .end();

                var hasAnySelected = $('.refinements').attr('data-hasanyselected');
                $('*[data-clear]')[(hasAnySelected === 'true') ? 'show':'hide']();
                event.preventDefault();
            });
        }};
});