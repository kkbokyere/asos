/**
 * @function handleCheckboxes
 * @memberOf asosRefinementsApp
 * @description this sets the stat of the parent panel hasselection data attribute.
 */
angular.module('asosRefinementsApp').directive('handleCheckboxes', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('change', function(event) {
                var inputEl, hasSelections, hasAnySelected;
                inputEl = $(event.target);
                hasSelections = inputEl.closest('ul').find(':checked').length;
                hasAnySelected = $('.refinements :checked').length;
                inputEl.closest('.panel').attr('data-hasselections', !!hasSelections);
                $('.refinements').attr('data-hasanyselected', !!hasAnySelected);

                //if no more input elements are selected in panel, then hide clear button.
                inputEl.closest('.panel').find('.clear-filter')[hasSelections ? 'show':'hide']();
                //if any selected, then show clear all
                //TODO: this could be improved by emit a event to the scope and reused by the click. or could move logic as part of directive
                $('[data-clear="all"]')[hasAnySelected ? 'show':'hide']();
            });
        }};
}]);