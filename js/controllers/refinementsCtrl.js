/**
 * @function asosRefinementsCtrl
 * @memberOf asosRefinementsApp
 * @description the main controller for the refinements application. It loads in the facets list model into the scope, for the view to use to generate the dom.
 */
angular.module('asosRefinementsApp', ['ngRoute'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/refinementsPanel.html',
                controller: 'asosRefinementsCtrl'
            });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

    }).controller('asosRefinementsCtrl', function ($scope, facetsListModel) {
        $scope.facets = facetsListModel;
        $scope.$on('deserialize', function() {
            var refinements = decodeURIComponent(location.search).slice(8).split('|');
            refinements[0] && $.each(refinements, function(i, panelData) {
                var panelName = panelData.split(':')[0],
                    panelValues = panelData.split(':')[1].split(',');
                switch(panelName) {
                    case 'size':
                        panelName = 'size';
                        break;
                    case 'colour':
                        panelName = 'base_colour';
                        break;
                    default:
                        panelName = 'brand';
                }
                selectRefinementValues(panelName,panelValues)
            });

            function selectRefinementValues(type, panelValues) {
                var refinementValues = $("[data-id="+type+"] [type=checkbox]");
                $.each(panelValues, function(i, val) {
                    refinementValues.filter('#' + type +'_' + decodeURIComponent(val)).attr('checked', true).change();
                });
            }
        });

});
