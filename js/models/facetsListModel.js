/**
 * @function facetsListModel
 * @memberOf asosRefinementsApp
 * @description this creates the data set with models the main view with panels for each type of facet.
 */
angular.module('asosRefinementsApp').service("facetsListModel", function() {
    var size = {
            'id':'size',
            'title':'Size',
            'hasselections': false,
            'displaystyle': 'single-column',
            'properties':[{
                'name': 'UK 4', 'id':'4'
            },
                {
                    'name': 'UK 6', 'id':'6'
                },
                {
                    'name': 'UK 8', 'id':'8'
                },
                {
                    'name': 'UK 10', 'id':'10'
                },
                {
                    'name': 'UK 12', 'id':'12'
                },
                {
                    'name': 'UK 14', 'id':'14'
                },
                {
                    'name': 'UK 16', 'id':'16'
                }
            ]
        },
        baseColour = {
            'id':'base_colour',
            'title':'Base Colour',
            'hasselections': false,
            'displaystyle': 'two-column',
            'properties':[{
                'name': 'Yellow', 'id':'1'
            },
                {
                    'name': 'White', 'id':'2'
                },
                {
                    'name': 'Red', 'id':'3'
                },
                {
                    'name': 'Purple', 'id':'4'
                },
                {
                    'name': 'Pink', 'id':'5'
                },
                {
                    'name': 'Orange', 'id':'6'
                },
                {
                    'name': 'Multi', 'id':'7'
                },
                {
                    'name': 'Grey', 'id':'8'
                },
                {
                    'name': 'Green', 'id':'9'
                },
                {
                    'name': 'Cream', 'id':'10'
                },
                {
                    'name': 'Brown', 'id':'11'
                },
                {
                    'name': 'Blue', 'id':'12'
                },
                {
                    'name': 'Black', 'id':'13'
                },
                {
                    'name': 'Beige', 'id':'14'
                }
            ]
        },
        brand = {
            'id':'brand',
            'title':'Brand',
            'hasselections': false,
            'displaystyle': 'single-column',
            'properties':[{
                'name': 'ASOS', 'id':'3098'
            },
                {
                    'name': 'Vero Moda', 'id':'716'
                },
                {
                    'name': 'TFNC', 'id':'2837'
                },
                {
                    'name': 'Paprika', 'id':'739'
                },
                {
                    'name': 'Motel', 'id':'9387'
                },
                {
                    'name': 'Aqua', 'id':'34567'
                },
                {
                    'name': 'Rare', 'id':'2039'
                }
            ]
        };

    return [size, baseColour, brand];
});